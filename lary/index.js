function isEmail(email) {
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (filter.test(email)) {
        return true; 
    } 
    else {
        return false;
    } 
}
function checkNumber(num){ 
    var reg = /^[0-9]+$/; 
    if(num!=""&&!reg.test(num)){  
        return false; 
    } 
} 
   
function Validation(){
    var Uemail = document.forms["theform"]["Uemail"].value;
    var Tnum = document.forms["theform"]["Tnum"].value;
    var bdate = document.forms["theform"]["birthdate"].value;
    
    if(isEmail(Uemail) == false){
        alert("plz enter correct email");
        return false;
    }
    if(checkNumber(Tnum)==false){
        alert("plz only input number");
        return false;
    }
   
}